﻿using System;
using System.Collections.Generic;
using System.Linq;
using Codefirst_Test.Models;
using Newtonsoft.Json.Linq;

namespace Codefirst_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var dbContext = new PersonDBContext();
            List<Person> listperson = new();
            var listPerson = dbContext.Persons.ToList();
            do
            {
                Console.WriteLine("0. Exit");
                Console.WriteLine("1. Read file json save to my SQL");
                Console.WriteLine("2. Edit name person");
                Console.WriteLine("3. Delete person");
                Console.WriteLine("4. print list of person in order A&B ");
                Console.WriteLine("Please choose: ");

                int choose = int.Parse(Console.ReadLine());

                switch (choose)
                {
                    case 0: break;
                    case 1:
                        listperson = ImportJson();
                        dbContext.AddRange(listperson);
                        dbContext.SaveChanges();
                        break;

                    case 2:
                        Updateperson();
                        break;

                    case 3:
                        Deleteperson();
                        break;

                    case 4:
                        OderPerson();
                        break;
                }
            } while (true);


        }
        private static void Updateperson()
        {
            try
            {
                var dbContext = new PersonDBContext();
                Console.Write("Enter the name of the person to be edited");
                string nameSearch = Console.ReadLine();
                Person psUpdate = dbContext.Persons.SingleOrDefault(p => p.Name == nameSearch);
                if (psUpdate != null)
                {
                    Console.Write("Enter name update ");
                    string nameEdit = Console.ReadLine();
                    string result = psUpdate.Name +" "+ nameEdit;
                    psUpdate.Name = result;
                    dbContext.Update(psUpdate);
                    dbContext.SaveChanges();
                } 
                else
                {
                    Console.Write("Person not found");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Update Error : " + ex.Message.ToString());
            }
        }

        private static void Deleteperson()
        {
            try
            {
                var dbContext = new PersonDBContext();
                Console.Write("Enter the name of the person to be delete");
                string nameSearch = Console.ReadLine();
                Person psUpdate = dbContext.Persons.SingleOrDefault(p => p.Name == nameSearch);
                if (psUpdate != null)
                {
                    Console.WriteLine("Are you sure you want to delete this person"+psUpdate.Name+" ?[y/n]");
                    string key = Console.ReadLine();
                    if (key == "y" || key == "Y")
                    {
                        dbContext.Remove(psUpdate);
                        dbContext.SaveChanges();
                    }
                    else if (key == "n" || key == "N")
                    {
                        return;
                    }
                }
                else
                {
                    Console.Write("Person not found");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Update Error : " + ex.Message.ToString());
            }
        }

        private static void OderPerson()
        {
            try
            {
                var dbContext = new PersonDBContext();
                var listPerson = dbContext.Persons.OrderBy(p=>p.Name).ToList();
                listPerson.ForEach(a =>
                {
                    Console.WriteLine($"Name---------{a.Name} \n Meaning-------{a.Meaning}");

                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("Update Error : " + ex.Message.ToString());
            }
        }

        static List<Person> ImportJson()  // doc file json StudentData
        {
            try
            {
                var content = System.IO.File.ReadAllText(@"Data.json");
                List<Person> listSD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Person>>(content);
                Console.WriteLine("Read file json successful");
                Console.WriteLine("Length: " + listSD.Count);
                return listSD;
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} Exception caught.", ex);
                throw;
            }
        }
    }
}
