﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codefirst_Test.Models
{
    class PersonDBContext : DbContext
    {
        public PersonDBContext()
        {

        }
        public PersonDBContext(DbContextOptions<PersonDBContext> options)
            : base(options)
        {

        }
        public virtual DbSet<Person> Persons { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-OTS9HKL\\SQLGIAPH;Database=PersonDB;Trusted_Connection=True;");
            }
        }
    }
}
